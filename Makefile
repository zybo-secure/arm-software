-include build.config
LIBS += timing.o gpio.o
LIBS += $(FILES)
CFLAGS += -Wall -Werror $(addprefix -D, $(DEFINES))
APP = controller

.PHONY: clean

all: clean build

build: $(APP)

$(APP): control.o $(LIBS)
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $(LDFLAGS) $< -o $@ $(LDLIBS)

clean:
	rm -rf $(APP) *.o
