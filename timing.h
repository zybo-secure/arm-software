#ifndef TIMING_H
#define TIMING_H
#include <time.h>
#include <sys/time.h>

#define PERIOD 50000 // period in useconds

#define BUFSIZE 100
void get_timing(char *buf);
#endif /* TIMING_H */
