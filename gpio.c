#include "gpio.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdio.h>
#include <unistd.h>

#define GPIO_ADDR 0x41200000
#define MAPPED_SIZE 0x1000

static volatile uint16_t *PIN_PTR;

int pin_setup(void) {
    int fd;
    fd = open("/dev/mem", O_RDWR | O_SYNC);
    if (fd < 0) {
        printf("Failed to open /dev/mem\n");
        return -1;
    }
    PIN_PTR = mmap(0, MAPPED_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, \
            fd, GPIO_ADDR);

    /* set to output */
    PIN_PTR[2] = 0x0;
    PIN_PTR[3] = 0x0;

    return fd;
}

void pin_cleanup(int fd) {
    munmap((void *)PIN_PTR, MAPPED_SIZE);
    close(fd);
}

void set_pin_high(uint16_t mask) {
    PIN_PTR[0] |= (0xFFFF & mask);
}

void set_pin_low(uint16_t mask) {
    PIN_PTR[0] &= ~(0xFFFF & mask);
}

