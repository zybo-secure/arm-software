#ifndef FIFO_H
#define FIFO_H

/* Main header for FIFO defines */
#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdint.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#define FIFO_SIZE 0x10000
#define FIFO_TX_BASE 0x40000000
#define FIFO_RX_BASE 0x41000000

// reset, vacancy, write, transmit
#define TX_RESET    0x08 / 4
#define TX_VACAN    0x0C / 4
#define TX_WRITE    0x10 / 4
#define TX_TRAN_LEN 0x14 / 4

// reset, occupancy, read, recv
#define RX_RESET    0x18 / 4
#define RX_OCCUP    0x1C / 4
#define RX_READ     0x20 / 4
#define RX_RECV_LEN 0x24 / 4

#define __TX 0
#define __RX 1

/* reset a fifo, Rx or Tx */
void resetFifo(volatile uint32_t *fifo, int dir);

/* fifo to write to, data to write, number of 8 bit words to write */
int16_t writeFifo(volatile uint32_t *fifoTx, int16_t *buf, int16_t len);

int fifoEmpty(volatile uint32_t *fifoRX);
/* fifo to read from, buffer to read into, size of buffer. Extra data will be thrown out */
/* returns number of 8 bit words read */
int16_t readFifo(volatile uint32_t *fifoRx, int16_t *buf, int16_t len);

/* open a fifo, store the address in *addr */
/* if fd is zero, open /dev/mem, else use fd */
int openFifo(int fd, volatile uint32_t **addr, off_t base);

/* if fd is nonzero, close the file */
int closeFifo(int fd, volatile uint32_t* addr);

#endif /* FIFO_H */ 
