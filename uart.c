#ifdef UART_DIRECT
#include "uart.h"
#include <stdio.h>
#include <termios.h>
#include <unistd.h>

static int config_uart(int dev) {
    struct termios config;
    if (!isatty(dev)) {
        printf("Error, dev is not a tty!\n");
        return 1;
    }
    if (tcgetattr(dev, &config) < 0) {
        printf("Error, unable to get attribute!\n");
        return 2;
    }
    printf("Setting tty to raw\n");
    config.c_iflag &= ~(IGNBRK | BRKINT | ICRNL |
                        INLCR | PARMRK | INPCK | ISTRIP | IXON);
    config.c_oflag = 0;
    config.c_lflag &= ~(ECHO | ECHONL | ICANON | IEXTEN | ISIG);
    config.c_cflag &= ~(CSIZE | PARENB);
    config.c_cflag |= CS8;
    config.c_cc[VMIN] = 1;
    config.c_cc[VTIME] = 0;

    printf("Setting tty speed\n");
    if (cfsetispeed(&config, B57600) < 0 || cfsetospeed(&config, B9600)) {
        printf("Error setting speed\n");
        return 3;
    }

    printf("Applying configuration\n");
    if(tcsetattr(dev, TCSAFLUSH, &config) < 0) {
        printf("Error applying configuration\n");
        return 4;
    }
    printf("Done\n");

    return 0;
}

static inline int16_t write_byte(int dev, int8_t *data) {
    return write(dev, data, 1);
}

static inline int16_t read_byte(int dev, int8_t *data) {
    return read(dev, data, 1);
}

static int tx, rx;

int init_devs(void) {
    tx = open("/dev/ttyUL1", O_WRONLY);
    if (config_uart(tx)) {
        printf("Error configuring tx\n");
        return 1;
    }
    rx = open("/dev/ttyUL2", O_RDONLY);
    if (config_uart(rx)) {
        printf("Error configuring rx\n");
        return 2;
    }
    return 0;
}

int16_t send_cmds(fixed16_t *buf, int16_t len) {
    int8_t temp;
    tcflush(tx, TCOFLUSH);
    temp = (int8_t)(len);
    while(!write_byte(tx, &temp));
    temp = (int8_t)(len >> 8);
    while(!write_byte(tx, &temp));
    for (int i = 0; i < len; i++) {
        temp = (int8_t)(buf[i]);
        while(!write_byte(tx, &temp));
        temp = (int8_t)(buf[i] >> 8);
        while(!write_byte(tx, &temp));
    }
    return len;
}

int16_t recv_data(fixed16_t *buf, int16_t len) {
    int16_t ilen = 0;
    int8_t temp;
    while(!read_byte(rx, &temp));
    ilen |= (((int16_t)temp) & 0x00FF);
    while(!read_byte(rx, &temp));
    ilen |= (((int16_t)temp << 8) & 0xFF00);
    if (ilen < len) {
        len = ilen;
    }
    for (int i = 0; i < len; i++) {
        buf[i] = 0;
        while(!read_byte(rx, &temp));
        buf[i] |= (((int16_t)temp) & 0x00FF);
        while(!read_byte(rx, &temp));
        buf[i] |= (((int16_t)temp << 8) & 0xFF00);
    }
    tcflush(rx, TCIFLUSH);
    return len;
}

int cleanup_devs(void) {
    int retval = 0;
    retval -= close(tx);
    retval = retval << 1;
    retval -= close(rx);
    return retval;
}

#endif
