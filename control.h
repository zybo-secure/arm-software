#ifndef CONTROL_H
#define CONTROL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define BUFSIZE 10

typedef int16_t fixed16_t;

/* API function to send (max) len floats of data in buffer buf */
/* Returns number of floats sent */
int16_t send_cmds(fixed16_t *buf, int16_t len);

/* API function to recieve (max) len floats into buffer buf */
/* Returns number of floats recieved */
int16_t recv_data(fixed16_t *buf, int16_t len);

/* API function to wait until the next timer period has elapsed */
/* Returns 1 if period has elapsed, else 0 */
int wait_period(void);

/* API function to init interfaces for communicating with UART */
/* Returns 0 if no errors, else positive integer */
int init_devs(void);

/* API function to clean up any actions that took place in init_devs */
/* Returns 0 if no errors, else integer */
int cleanup_devs(void);

/* API function to initialize the timer used for periodicity */
/* Returns 0 if no errors, else integer */
int init_timer(void);

/* API function to clean up any actions that took place in init_timer */
/* Returns 0 if no errors, else an integer */
int cleanup_timer(void);

/* API function to run PID control */
/* Takes a input array of floats and number of values */
/* writes output to array of floats and number written */
int16_t pid(fixed16_t *outputs, fixed16_t *inputs, int16_t len);

/* Turns a float to a fixed16_t */
float fixed_to_float(fixed16_t f);

/* Turns a fixed16_t to a float */
fixed16_t float_to_fixed(float f);

#endif /* CONTROL_H */
