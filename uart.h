#ifndef UART_H
#define UART_H

/* Main header for FIFO defines */
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include "comm.h"

typedef int16_t fixed16_t;

/* configure the tty for a given uart */
static int config_uart(int dev);

#endif /* UART_H */
