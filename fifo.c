#ifdef UART_FIFO
#include "fifo.h"
#include <stdlib.h>

static void reset_fifo(volatile uint32_t *fifo, int dir) {
    int offset = (dir == __TX) ? TX_RESET : RX_RESET;
    fifo[offset] = 0xA5;
}

/* fifo to write to, data to write, number of 8 bit words to write */
static int16_t write_fifo(volatile uint32_t *fifoTx, int16_t *buf, 
        int16_t len) {
    int16_t sent = 0;
    int16_t ilen = (len / 2);
    ilen += (len % 2) ? 1 : 0;
    uint32_t temp;
    for (int i = 0; i < len; i++) {
        temp = 0;
        temp |= ((uint32_t)(buf[i]) & 0x0000FFFF);
        if (++i < len) {
            temp |= (((uint32_t)(buf[i]) << 16) & 0xFFFF0000);
        }
        if (fifoTx[TX_VACAN] < 4) {
            fifoTx[TX_TRAN_LEN] = (ilen - sent) * 4;
            sent = i / 2;
        }
        fifoTx[TX_WRITE] = temp;
    }
    fifoTx[TX_TRAN_LEN] = (ilen - sent) * 4;
    return len;
}

static int fifo_empty(volatile uint32_t *fifoRx) {
    return !(fifoRx[RX_OCCUP]);
}

static int16_t read_fifo(volatile uint32_t *fifoRx, int16_t *buf, 
        int16_t len) {
    int i;
    while (fifo_empty(fifoRx));
    int16_t ilen = (int16_t) (fifoRx[RX_RECV_LEN]);
    ilen /= 4;
    int32_t temp;
    if (ilen * 2 < len) {
        len = ilen * 2;
    }
    for (i = 0; i < len; i++) {
        temp = fifoRx[RX_READ];
        buf[i] = (int16_t)temp;
        if (++i < len) {
            buf[i] = (int16_t)(temp >> 16);
        }
    }
    while (i < ilen) {
        fifoRx[RX_READ];
        i++;
    }
    return len;
}

static int open_fifo(int fd, volatile uint32_t **addr, off_t base) {
    if (!fd) {
        fd = open("/dev/mem", O_RDWR);
        if (fd < 1) {
            int errnum = errno;
            printf("Error opening /dev/mem %d: %s\n", errno, 
                    strerror(errnum));
            return -1;
        }
    }
    *addr = (uint32_t *)mmap(NULL, FIFO_SIZE, PROT_READ|PROT_WRITE, 
            MAP_SHARED, fd, base);
    if (!*addr) {
        printf("mmap failed!\n");
        return -2;
    }

    return fd;
}

static int close_fifo(int fd, volatile uint32_t* addr) {
    if (munmap((void *)addr, FIFO_SIZE)) {
        printf("munmap failed!\n");
        return -1;
    }
    if (fd) {
        return close(fd);
    }
    return 0;
}

/* API functions */

static volatile uint32_t *txfifo, *rxfifo;
static int fd;

int init_devs(void) {
    txfifo = NULL;
    rxfifo = NULL;

    printf("Opening fifos\n");
    fd = open_fifo(0, &txfifo, FIFO_TX_BASE);
    open_fifo(fd, &rxfifo, FIFO_RX_BASE);

    printf("Reseting fifos\n");
    reset_fifo(txfifo, __TX);
    reset_fifo(rxfifo, __RX);

    return 0;
}

int16_t send_cmds(int16_t *buf, int16_t len) {
    return write_fifo(txfifo, buf, len);
}

int16_t recv_data(int16_t *buf, int16_t len) {
    return read_fifo(rxfifo, buf, len);
}

int cleanup_devs(void) {
    int retval = 0;
    retval -= close_fifo(0, txfifo);
    retval = retval << 1;
    retval -= close_fifo(fd, rxfifo);
    return retval;
}

#endif
