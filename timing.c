#include "timing.h"
#include <string.h>
#include <stdio.h>

void get_timing(char *buf) {
    static struct timespec start, end;
    char str[BUFSIZE];
    clock_gettime(CLOCK_REALTIME, &end);
    if (buf) {
        sprintf(str, "%lld.%.9ld,", (long long) 
                (end.tv_sec - start.tv_sec),
                (end.tv_nsec - start.tv_nsec));
        strcat(buf, str);
    }
    start.tv_sec = end.tv_sec;
    start.tv_nsec = end.tv_nsec;
}
