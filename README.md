# arm-software
This repository contains software that runs on the arm processor of the Zybo Z7
Some of the software is duplicated from the repo ipc-zybo, i.e. the two tools, ipc-list and ipc-close

Data transmission over uart is of the format <# of integers,integers> in little endian (LSB first) mode.
