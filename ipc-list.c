#include "comm.h"

#define MAX_KEY 1024
int main(int argc, char **argv) {
    int msqid;

    printf("IPC's currently open:\n");
    for (key_t key = 0; key < MAX_KEY; key++) {
        msqid = msgget(key, 0);
        if (msqid >=0 ) {
            printf("%d\n", key);
        }
    }
        
    printf("That's it.\n");
    
    return 0;
}
