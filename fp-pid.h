#ifndef FP_PID_H
#define FP_PID_H

#include <stdio.h>

#define MAX_VAL 64

typedef float data_type;
static const data_type COEFF[] = {
    0.5112,
    6.0324,
    -0.7931,
    35.3675,
    MAX_VAL - 1,
    -MAX_VAL
};

#endif /* FP_PID_H */
