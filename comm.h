#ifndef COMM_H
#define COMM_H

#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#define DATA_SIZE 128

#define UART_GET 1
typedef struct {
    long mtype;
    char data[DATA_SIZE];
} uart_get;

#define UART_SET 2
typedef struct {
    long mtype;
    char data[DATA_SIZE];
} uart_set;

typedef union {
    uart_set set;
    uart_get get;
} uart_msg;

#define msgsize(msg) (sizeof(msg) - sizeof(long))

#endif /* COMM_H */
