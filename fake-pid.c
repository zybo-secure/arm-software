#ifdef FAKE_PID
#include "control.h"
#include <string.h>

/* Takes in the input data and calculates the output data */
int16_t pid(fixed16_t *outputs, fixed16_t *inputs, int16_t len) {
    float converted[2];
    printf("SET:\n");
    for (int i = 0; i < len; i++) {
        converted[i] = fixed_to_float(inputs[i]);
        outputs[i] = float_to_fixed(converted[i]) + float_to_fixed(1);
        printf("\t%f\n", fixed_to_float(outputs[i]));
    }
    return len;
}

#endif
