#include <errno.h>
#include <unistd.h>
#include <signal.h>
#include "control.h"
#include "gpio.h"
#include <stdio.h>

static volatile int shutdown = 0;

static void handler(int val) {
    shutdown = 1;
}

enum {STATE_SLEEP, STATE_GET_S, STATE_GET_R, STATE_PID, 
        STATE_SET_S} STATE;

#define FIXED_POINT_BITS 5
float fixed_to_float(fixed16_t f) {
    return ((float) f / (float)(1 << FIXED_POINT_BITS));
}

fixed16_t float_to_fixed(float f) {
    return (fixed16_t)(f * (1 << FIXED_POINT_BITS));
}

void loop() {
    int retval;
    int16_t numrecv, numsent;
    fixed16_t sendcmds[BUFSIZE], recvdata[BUFSIZE];

    shutdown = 0;
    shutdown += init_devs();
    /* wait to init timer until first data is received */
    numrecv = recv_data(recvdata, BUFSIZE);
    send_cmds(recvdata, numrecv);
    shutdown += init_timer();

    while (!shutdown) {
        numrecv = 0;
        numsent = 0;
        /* sleep */
        STATE = STATE_SLEEP;
        set_pin_low(GPIO_MASK);
        while (!wait_period());

        /* get start time */
        set_pin_high(GPIO_START);

        /* send get message */
        STATE++;
        set_pin_high(GPIO_GET);
        numrecv = recv_data(recvdata, BUFSIZE);
        set_pin_high(GPIO_GETD);

        /* do "PID" loop */
        STATE++;
        set_pin_high(GPIO_PID);
        numsent = pid(sendcmds, recvdata, numrecv);
        set_pin_high(GPIO_PIDD);

        /* send set message */
        set_pin_high(GPIO_SET);
        retval = send_cmds(sendcmds, numsent);
        set_pin_high(GPIO_SETD);
        retval++;
    }
    if (cleanup_devs() || cleanup_timer()) {
        printf("Error cleaning up!\n");
    }
}

int main(int argc, char **argv) {
    int memfd;

    memfd = pin_setup();

    signal(SIGINT, handler);
    loop();

    printf("Error in the main control loop...\n");

    pin_cleanup(memfd);

    return 0;
}
