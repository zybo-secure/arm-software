#ifdef LINUX_TIMER
#include <time.h>
#include <signal.h>
#include <sys/time.h>

static unsigned int periodNum;

static void alarm_handler(int val) {
    periodNum++;
}

int wait_period(void) {
    static unsigned int oldPeriodNum = 0;
    if (periodNum > oldPeriodNum) {
        oldPeriodNum = periodNum;
        return 1;
    } else {
        return 0;
    }
}

int init_timer(void) {
    struct itimerval period_timer;
    struct timeval period_tv;
    period_tv.tv_sec = 0;
    period_tv.tv_usec = 50000;
    period_timer.it_interval = period_tv;
    period_timer.it_value = period_tv;
    periodNum = 0;

    setitimer(ITIMER_REAL, &period_timer, NULL);
    signal(SIGALRM, alarm_handler);
    
    return 0;
}

int cleanup_timer(void) {
    return 0;
}

#endif
