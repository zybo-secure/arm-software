#ifndef AXI_TIMER_H
#define AXI_TIMER_H

#include <stdio.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <stdint.h>
#include <unistd.h>

#define TIMER_BASE  0x42800000
#define TIMER_SIZE  0x10000
#define TIMER_CTRL  0x0 / 4
#define TIMER_COUNT 0x8 / 4
#define TICKS_TO_MS 50000
#define CTRL_VAL    0x0691

#define PERIOD 50

#endif /* AXI_TIMER_H */
