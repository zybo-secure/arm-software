#ifndef GPIO_H
#define GPIO_H

#include <stdint.h>

#define GPIO_MASK   0xffff
#define GPIO_START  0x0001
#define GPIO_GET    0x0002
#define GPIO_GETD   0x0004
#define GPIO_PID    0x0008
#define GPIO_PIDD   0x0010
#define GPIO_SET    0x0020
#define GPIO_SETD   0x0040

int pin_setup(void);

void pin_cleanup(int fd);

void set_pin_high(uint16_t mask);

void set_pin_low(uint16_t mask);

#endif /* GPIO_H */
