#include "comm.h"

int main(int argc, char **argv) {
    key_t key;
    int msqid;
    if (argc < 2) {
        printf("Usage: %s key\n", argv[0]);
        return 1;
    }
    for (int i = 1; i < argc; i++) {
        key = strtol(argv[i], NULL, 10);

        msqid = msgget(key, 0);
    
        if (msqid < 0) {
            printf("IPC with key %d does not exist\n", key);
            continue;
        }
    
        if (msgctl(msqid, IPC_RMID, NULL)) {
            printf("Unable to close IPC with key %d\n", key);
            continue;
        }
        printf("Closed IPC with key %d\n", key);
    }
    
    return 0;
}
