#ifdef AXI_TIMER
#include "axi-timer.h"

static int fd;
static volatile uint32_t *timer;


int wait_period(void) {
    static unsigned int oldPeriodNum = 0;
    unsigned int periodNum;
    periodNum = timer[TIMER_COUNT] / TICKS_TO_MS;
    if ((periodNum / PERIOD) > (oldPeriodNum / PERIOD)) {
        oldPeriodNum = periodNum;
        return 1;
    } else {
        return 0;
    }
}

int init_timer(void) {
    fd = open("/dev/mem", O_RDWR);
    if (fd < 1) {
        printf("Error opening /dev/mem\n");
        return -1;
    }
    timer = (uint32_t *)mmap(NULL, TIMER_SIZE, PROT_READ|PROT_WRITE,
            MAP_SHARED, fd, TIMER_BASE);
    if (!timer) {
        printf("mmap failed\n");
        return -2;
    }

    printf("Initializing timer\n"); 
    timer[TIMER_CTRL] = CTRL_VAL;
    printf("Done\n");
    
    return 0;
}

int cleanup_timer(void) {
    if (munmap((void *)timer, TIMER_SIZE)) {
        printf("munmap failed!\n");
        return -1;
    }
    return close(fd);
}

#endif
