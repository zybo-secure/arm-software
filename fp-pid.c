#ifdef FP_PID
#include "fp-pid.h"
#include "control.h"

int16_t pid(fixed16_t *outputs, fixed16_t *inputs, int16_t len) {
	// previous PID states: Yd(n-1), X1(n-1), X2(n-1), Yi(n-1)
	static data_type prev_x1 = 0, prev_x2 = 0, prev_yd = 0, prev_yi = 0;
	// current local states
	data_type w, e, y, x1, x2, yd, yi;
	// local variables
	data_type max_lim, min_lim, Gi, Gd, C, Gp, tmp;
	data_type pid_mult, pid_addsub, pid_mult2, pid_addsub2;
	
	// get PID input coefficients
	Gi = COEFF[0]; 	Gd = COEFF[1];
	C  = COEFF[2];  Gp = COEFF[3];
	max_lim = COEFF[4]; min_lim = COEFF[5];

	// get PID input signals
    if (len < 2) {
        outputs[0] = float_to_fixed(3.14);
        return 1;
    } else {
        w = fixed_to_float(inputs[0]);
        y = fixed_to_float(inputs[1]);
    }
	// w = effective input signal
	// y = closed loop signal

	// compute error signal E = W - Y
	pid_addsub = w - y;
	e          = (pid_addsub > max_lim) ? max_lim : pid_addsub;
	e          = (pid_addsub < min_lim) ? min_lim : e;

	pid_mult  = Gd * e;
	pid_mult2 = Gi * e;
	x1  = pid_mult;  // input signal of the derivative stage
	x2  = pid_mult2; // input signal of the integration stage

	// Derivation stage
	// Yd(n) = -C*Yd(n-1)+X1(n)-X1(n-1) = X1 - (prev_X1 + C*prev_Yd)
	pid_mult   = C * prev_yd;
	pid_addsub2= x1 - prev_x1; 
	pid_addsub = pid_addsub2 -pid_mult;
	yd         = pid_addsub;

	// Integrator  stage
	// Ti = X2(n) + X2(n-1) 
	// Yi(n) = CLIP( Yi(n-1) + Ti )    
	pid_addsub = prev_x2 + x2;
	pid_addsub2= prev_yi + pid_addsub;
	yi         = (pid_addsub2 > max_lim) ? max_lim : pid_addsub2;
	yi         = (pid_addsub2 < min_lim) ? min_lim : yi;

	// output signal U(n)
	pid_mult   = Gp * e;
    pid_addsub = yi + yd;
	pid_addsub2= pid_addsub + pid_mult;
	tmp        = (pid_addsub2 > max_lim) ? max_lim : pid_addsub2;
	tmp        = (pid_addsub2 < min_lim) ? min_lim : tmp;
    // tmp = PID output
	// e = error reported as output
    outputs[0] = float_to_fixed(tmp);
    outputs[1] = float_to_fixed(e);

	// update internal PID states for the next iteration
	prev_x1 = x1; prev_x2 = x2;
	prev_yd = yd; prev_yi = yi;

	return 2;
}
#endif
